# konga-ui



## Getting started

Konga in this repository is a simple one which only store data inside their pods, because we use localhost mongo db in the same pods. To use more persistent data, please look for the solution by yourself in internet.

## How to Set Up

- [ ] kubectl apply -f konga.yaml -n yournamespace
- [ ] wait until konga is up and running
- [ ] setup your load balancer / ingress to access konga port mentioned in konga service in your cluster
- [ ] access your load balancer public IP to start using konga
